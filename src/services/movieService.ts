import axios from '../axios';
import { Login } from '../models/User';

export default class MovieService {
    options:any;
    constructor(options: any){
        this.options = options;
    }
    
    static searchMovie = async (query: string) => {
        return axios.get("/movie/search",{
            params: {
                query
            }
        })
    }
    static getFavourites = async () => {
        return axios.get("/movie/favourites")
    }
    static explore = async (page: string) => {
        return axios.get("/movie/explore/"+page);
    }
    static favourite = async (id: number) => {
        return axios.get("/movie/favourite/"+id);
    }
}