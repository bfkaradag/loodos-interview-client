import axios from '../axios';
import { Login, Signup } from '../models/User';

export default class UserService {
    options:any;
    constructor(options: any){
        this.options = options;
    }
    
    static create = (data:Signup) => {
        return axios.post("/user/create", data)
    }
    
}