import axios from '../axios';
import { Login, Signup } from '../models/User';

export default class AuthService {
    options:any;
    constructor(options: any){
        this.options = options;
    }
    
    static login = (data: Login) => {
        return axios.post("/auth/login", data)
    }
    static authCheck = async () => {
        return await axios.get("/auth/check")
    }
}