import { useNavigate } from "react-router";

let _navigator: any;

function setTopLevelNavigator(navigatorRef: any) {
  _navigator = navigatorRef;
}

function _navigate(routeName: string) {
    const navigate = useNavigate();
   _navigator.dispatch(
    navigate(routeName)
  );
}
export default {
  _navigate,
  setTopLevelNavigator,
};