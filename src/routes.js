import {useState, useEffect} from 'react';
import {Navigate, Routes, Route, useNavigate} from 'react-router-dom';
import AuthService from './services/authService';
import Login from './pages/login/login'
import Signup from './pages/login/signup';
import Panel from './pages/panel';
import Home from './pages/Home';
import MoviesResults from './pages/panel/MoviesResults';
import Favourites from './pages/panel/Favourites';
const PageRoutes = () => {   
    return(
        <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/panel" element = {<Panel />} />
            <Route path="/panel/search" element = {<MoviesResults />} />
            <Route path="/panel/favourites" element = {<Favourites />} />
        </Routes>
    )
}

export default PageRoutes;