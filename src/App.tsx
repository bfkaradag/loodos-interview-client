import React from 'react';
import logo from './logo.png';
import './App.css';
import Routes from './routes';
import { ChakraProvider, extendTheme } from "@chakra-ui/react";

const colors = {
  input : {
    100: "#c4c4c42a"
  }
}

const theme = extendTheme({ colors })

function App() {
  return (
    <ChakraProvider theme={theme}>
      <Routes />
    </ChakraProvider>
  );
}

export default App;
