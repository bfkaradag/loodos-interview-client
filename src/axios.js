import axios from "axios";
import { useNavigate } from "react-router-dom";
import { API_ADDRESS } from "./environments";
import navigator from "./navigator";

const _axios = axios.create({
    baseURL: API_ADDRESS,
    withCredentials: false,
    headers:{
        "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      "Access-Control-Allow-Methods":"GET,PUT,POST,DELETE,PATCH,OPTIONS",
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
      "Authorization": sessionStorage.getItem("jwttimeout") == "false" ? sessionStorage.getItem("jwtToken") : ""

    }
});

_axios.interceptors.response.use(response => {
  return response
}, error => {
  if(error.response.status === 401) {
    window.location = "/login"
  } 
  return Promise.reject(error);
})

export default _axios;