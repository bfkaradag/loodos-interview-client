import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import Splash from '../components/Splash'
import AuthService from "../services/authService";

export default () => {
    const navigate = useNavigate();
    useEffect(() => {
        AuthService.authCheck()
        .then(d => {
            setTimeout(() => {
                navigate("/panel")
            }, 1500)
        })
        .catch(err => {
            setTimeout(() => {
                navigate("/login")
            }, 1500)
        });
    },[]);
    return <Splash /> //TODO En son splash c
}