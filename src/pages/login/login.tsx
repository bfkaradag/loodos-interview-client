import { Button, Input, InputGroup, InputRightElement, Stack } from '@chakra-ui/react';
import {ViewIcon, ViewOffIcon} from '@chakra-ui/icons';
import {useEffect, useState} from 'react';
import LoginLayout from '../../layout/login_layout';
import AuthService from '../../services/authService';
import Response from '../../models/Response';
import {Link, Navigate, useNavigate} from 'react-router-dom'
import ResponseMessage from '../../components/ResponseMessage';

export default () => {
    const navigate = useNavigate();
    const [response,setResponse] = useState(); 
    const [showResponse, setShowResponse] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const handleClick = () => setShowPassword(!showPassword);
    const handleEmailChange = (e: any) => setEmail(e.target.value);
    const handlePasswordChange = (e: any) => setPassword(e.target.value);

    useEffect(() => {
        if(sessionStorage.getItem("jwttimeout") !== "true"){
            AuthService.authCheck()
            .then(d => navigate("/panel"))
            .catch(err => {
                sessionStorage.setItem("jwttimeout", "true")
            });
        }
        
    },[]);

    const login = async () => {
        const payload = {
            email: email,
            password: password
        }
        AuthService.login(payload)
        .then(response => {
            if(response.data.statusCode == 200) {
                navigate("/panel");
                sessionStorage.setItem("jwttimeout", "false");
                sessionStorage.setItem("jwtToken", `Bearer ${response.data.data.token}`);
                sessionStorage.setItem("firstName", response.data.data.firstName);
                sessionStorage.setItem("lastName", response.data.data.lastName);        
                sessionStorage.setItem("createdDate", response.data.data.createdDate);        
                
            }
        })
        .catch(error => {
            setShowResponse(true)
            setResponse(error.response)
        })
        .finally(() => {
            setTimeout(() => {
                setShowResponse(false)
            }, 2000)
        })
    }

    return(
       <LoginLayout>
           <div id="login-container">
               <h1>Welcome</h1>
               <Stack spacing="3" mt={"36px"} mb={"16px"}>
                <Input 
                    type="email" 
                    className="text-input" 
                    variant="outline" 
                    placeholder="Your Mail" 
                    size="lg"
                    onChange = {handleEmailChange}
                />
                <InputGroup mt={"26px !important"}>
                <Input
                    type={showPassword ? "text" : "password"}
                    placeholder="Password"
                    className="text-input"
                    size="lg"
                    onChange = {handlePasswordChange}
                />
                <InputRightElement style={{cursor:"pointer"}} width="4.5rem" onClick = {handleClick}>
                    {showPassword ?<ViewOffIcon fontSize="18px" color="lightgray" /> : <ViewIcon fontSize="18px" color="lightgray"/>}
                </InputRightElement>
                </InputGroup>
               </Stack>
               
               <p>
               Forget Password ?
               </p>
                {response && <ResponseMessage response = {response} show = {showResponse} />}
               
               <Button 
                colorScheme="blue"
                isFullWidth
                mt={"24px"}
                disabled = {(email == "" || password == "") ? true : false}
                onClick = {login}
                > LOGIN                        
                </Button>
                <div className="w-100 d-flex" style={{marginTop:29}}>
                    <Button
                    variant="text"
                    color="white"
                    m="auto"
                    >
                        <Link to="/signup">REGISTER</Link>
                    </Button>
                </div>
           </div>
       </LoginLayout>
    )

}