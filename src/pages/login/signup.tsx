import { Button, Input, InputGroup, InputRightElement, Stack } from '@chakra-ui/react';
import {ViewIcon, ViewOffIcon} from '@chakra-ui/icons';
import {useEffect, useState} from 'react';
import LoginLayout from '../../layout/login_layout';
import AuthService from '../../services/authService';
import Response from '../../models/Response';
import {Link, Navigate, useNavigate} from 'react-router-dom'
import ResponseMessage from '../../components/ResponseMessage';
import UserService from '../../services/userService';

export default () => {
    const navigate = useNavigate();
    const [response,setResponse] = useState(); 
    const [showResponse, setShowResponse] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showPassword2, setShowPassword2] = useState(false);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const handleClick = () => setShowPassword(!showPassword);
    const handleEmailChange = (e: any) => setEmail(e.target.value);
    const handlePasswordChange = (e: any) => setPassword(e.target.value);
    const handlePassword2Change = (e: any) => setPassword2(e.target.value);

    const handleFirstNameChange = (e: any) => setFirstName(e.target.value);
    const handleLastNameChange = (e: any) => setLastName(e.target.value);

    const disableBtn = password !== password2 || firstName == "" || lastName == "" || email == "";

    useEffect(() => {
        if(sessionStorage.getItem("jwttimeout") !== "true"){
            AuthService.authCheck()
            .then(d => navigate("/panel"))
            .catch(err => {
                sessionStorage.setItem("jwttimeout", "true")
            });
        }
        
    },[]);

    const signup = async () => {
        const payload = {
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
        }
        UserService.create(payload)
        .then(response => {
            if(response.data.statusCode == 200) {
                navigate("/login");  
            }
        })
        .catch(error => {
            setShowResponse(true)
            setResponse(error.response)
        })
        .finally(() => {
            setTimeout(() => {
                setShowResponse(false)
            }, 2000)
        })
    }

    return(
       <LoginLayout>
           <div id="login-container">
               <h1>Sign up</h1>
               <Stack spacing="3" mt={"36px"}>
                <Input 
                    type="email" 
                    className="text-input" 
                    variant="outline" 
                    placeholder="Your Mail" 
                    size="lg"
                    onChange = {handleEmailChange}
                />
                <InputGroup mt={"26px !important"}>
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Password"
                        className="text-input"
                        size="lg"
                        onChange = {handlePasswordChange}
                    />
                    <InputRightElement style={{cursor:"pointer"}} width="4.5rem" onClick = {handleClick}>
                        {showPassword ?<ViewOffIcon fontSize="18px" color="lightgray" /> : <ViewIcon fontSize="18px" color="lightgray"/>}
                    </InputRightElement>
                </InputGroup>
                <InputGroup mt={"26px !important"}>
                    <Input
                        type="password"
                        placeholder="Password"
                        className="text-input"
                        size="lg"
                        onChange = {handlePassword2Change}
                    />
                </InputGroup>
               </Stack>
               {
                   password !== password2 
                
                   ?
                   <p>Passwords don't match</p>
                   :
                   null
               }
               <Input 
                    mt={"26px !important"}
                    type="text" 
                    className="text-input" 
                    variant="outline" 
                    placeholder="First Name" 
                    size="lg"
                    onChange = {handleFirstNameChange}
                />
                <Input
                    mt={"26px !important"}
                    mb={"16px !important"}
                    type="text" 
                    className="text-input" 
                    variant="outline" 
                    placeholder="Last Name" 
                    size="lg"
                    onChange = {handleLastNameChange}
                />
               <p>
               Already have an acount ? <Link to="/login" style={{color:"deepskyblue", fontWeight:"bold"}}>login</Link>
               </p>
                {response && <ResponseMessage response = {response} show = {showResponse} />}
               
               <Button 
                colorScheme="blue"
                isFullWidth
                mt={"24px"}
                disabled = {disableBtn ? true : false}
                onClick = {signup}
                > SIGNUP                        
                </Button>
                
           </div>
       </LoginLayout>
    )

}