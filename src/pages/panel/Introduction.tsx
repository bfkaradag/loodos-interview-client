import { Button } from "@chakra-ui/button"
import { StarIcon } from "@chakra-ui/icons"
import { BASE_ADDRESS } from "../../environments";

export default () => {
    const bg = `linear-gradient(269.96deg, rgba(29, 29, 29, 0) 0.04%, rgba(29, 29, 29, 0.8) 99.5%), url(${BASE_ADDRESS}/static/bg/main.png)`;

    return(
        <>
        <div className="introduct-bg" style={{backgroundImage: bg}}/>
            <section className="introduct" >
                <div className="introduct-content" >
                    <div className="introduct-tag">
                        <span>Science Fiction</span>
                    </div>
                    <div className="introduct-rating">
                        <StarIcon color="white" />
                        <StarIcon color="white" />
                        <StarIcon color="white" />
                        <StarIcon color="gray" />
                        <StarIcon color="gray" />
                    </div>
                    <div className="introduct-title">
                        <h1>Godzilla vs. Kong</h1>
                    </div>
                    <div className="introduct-explain">
                        <p>
                        In a time when monsters walk the Earth, humanity’s fight for its future sets Godzilla and Kong on a collision course that will see the two most powerful forces of nature on the planet collide in a spectacular battle for the ages.
                        </p>
                    </div>
                    <div className="introduct-button">
                        <Button className="primary-btn">
                            WATCH NOW
                        </Button>
                    </div>
                </div>
                <div className="introduct-pass"></div>
            </section>
        </>
    )
}