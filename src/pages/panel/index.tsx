import { Button, Input, InputGroup, InputRightElement, Stack } from '@chakra-ui/react';
import {StarIcon} from '@chakra-ui/icons';
import {useEffect, useState} from 'react';
import PanelLayout from '../../layout/panel_layout';
import { BASE_ADDRESS } from '../../environments';
import Introduction from './Introduction';
import Explore from './Explore';
import Pagination from '../../components/Pagination';
import MoviesResults from './MoviesResults';
import AuthService from '../../services/authService';
import { useNavigate } from 'react-router';

export default () => {
    const navigate = useNavigate();
    const handlePagination = async (page: number) => setPage(page);
    const [page, setPage] = useState(1);
    useEffect(() => {
        if(sessionStorage.getItem("jwttimeout") === "true"){
            AuthService.authCheck()
            .then(d => navigate("/login"))
        }        
    },[]);
    return(
       <PanelLayout>            
        <main>            
            <Introduction />           
            <Explore page = {page} />
            <Pagination handlePagination = {(e: any) => handlePagination(e)} count = {500}/>
        </main>        

       </PanelLayout>
    )

}