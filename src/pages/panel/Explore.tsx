import { useState, useEffect } from "react";
import MovieCard from "../../components/MovieCard"
import Splash from "../../components/Splash";
import Movie from "../../models/Movie";
import MovieService from "../../services/movieService";
export default (props: any) => {
    const [movies, setMovies] = useState<Movie[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    
    useEffect(()  => {
        setIsLoading(false);    
        MovieService.explore(props.page.toString())
        .then(response => {
        setMovies(response.data.data)
    })
    .finally(() => setIsLoading(true))
    },[props.page])

    if(isLoading){
        return(
            <section className="explore">
                <h1>Trending</h1>
                <div className="movies d-flex row">
                    {
                        movies.map(m => {
                            return <MovieCard { ...m} />
                        })
                    }
                </div>
            </section>
        )
    }
    return <Splash />
}