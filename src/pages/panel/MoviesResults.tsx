import MovieCard from "../../components/MovieCard"
import PanelLayout from "../../layout/panel_layout"
import { useSearchParams } from "react-router-dom"
import MovieService from "../../services/movieService"
import Movie from "../../models/Movie"
import { useEffect, useState } from "react"
import Splash from "../../components/Splash"

export default () => {
    const [movies, setMovies] = useState<Movie[]>([]);
    const [searchParams, setSearchParams] = useSearchParams();
    const [isLoading, setIsLoading] = useState(false);
    const query = searchParams.get("query") || "";
    
    useEffect(()  => {
        MovieService.searchMovie(query)
        .then(response => {
        setMovies(response.data.data)
    })
    .finally(() => {
        setTimeout(() => {
            setIsLoading(true)
        },500)
    })
    },[searchParams])
    
    if(!isLoading) return <Splash />

    return(
        <PanelLayout>
            <div className="movies-results">
                <h1>Search  Results: {query}</h1>
                <hr className="title-hr" />
                <div className="movies d-flex row" style={{marginTop:60}}>
                    {
                        movies?.map(m => {
                            return <MovieCard {...m} />
                        })
                    }
                </div>
            </div>
        </PanelLayout>
    )
}

