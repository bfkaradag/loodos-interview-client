import { BASE_ADDRESS } from './environments';
const base = `${BASE_ADDRESS}/static/icons`;

class Theme {
     static icons = {
        heartFill: `${base}/heart-fill.svg`,
        heart: `${base}/heart.svg`,
        filmstrip: `${base}/filmstrip.svg`,
        tv: `${base}/tv.svg`,
        house: `${base}/house.svg`,
        close: `${base}/close.svg`,
        logo: `${base}/logo.png`,
        movieflix:`${base}/movieflix.svg`
    }
}
export default Theme;