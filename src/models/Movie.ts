export default class Movie {
    public id!: number;
    public title!: string;
    public isFavourite!: boolean;
    public releaseDate!: string;
    public imgUrl!: string;
}
export class IMovie {
    public adult!: boolean;
    public backdrop_path!: string;
    public belongs_to_collection!: null;
    public budget!: number;
    public genres!: Genre[];
    public homepage!: string;
    public id!: number;
    public imdb_id!: string;
    public original_language!: string;
    public original_title!: string;
    public overview!: string;
    public popularity!: number;
    public poster_path!: string;
    public production_companies!: ProductionCompany[];
    public production_countries!: ProductionCountry[];
    public release_date!: Date;
    public revenue!: number;
    public runtime!: number;
    public spoken_languages!: SpokenLanguage[];
    public status!: string;
    public tagline!: string;
    public title!: string;
    public video!: boolean;
    public vote_average!: number;
    public vote_count!: number;
}

export interface Genre {
    id:   number;
    name: string;
}

export interface ProductionCompany {
    id:             number;
    logo_path:      null | string;
    name:           string;
    origin_country: string;
}

export interface ProductionCountry {
    iso_3166_1: string;
    name:       string;
}

export interface SpokenLanguage {
    english_name: string;
    iso_639_1:    string;
    name:         string;
}
