import Movie from "./Movie";

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    favourites: Movie[];
}

export class Login {
    public email: string = "";
    public password: string = "";
}

export class Signup extends Login {    
    public firstName: string = "";
    public lastName: string = "";
}