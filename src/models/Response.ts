export default class Response{
    public data: any;
    public statusCode: number = 0;
    public statusMessage:string = "";
}

