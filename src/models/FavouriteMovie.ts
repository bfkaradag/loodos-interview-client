export default interface FavouriteMovie {
    id: number;
    userId: number;
    movieId: number;
    createdAt: Date;
}