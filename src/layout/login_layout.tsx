import { useEffect, useState} from 'react';
import Splash from '../components/Splash';
import {BASE_ADDRESS} from '../environments';
import Theme from "../theme";

export default ({children}: any) => {
    const [isLoad, setIsLoad] = useState(false);
    useEffect(() => {
        setTimeout(() => {
            setIsLoad(true);
        }, 500)
    },[])
    const bg = `linear-gradient(269.96deg, rgba(29, 29, 29, 0) 0.04%, rgba(29, 29, 29, 0.8) 99.5%), url(${BASE_ADDRESS}/static/bg/login.png)`;
    // return(
    //     <>
    //         <Splash />
    //         <div id="login-layout" style={{backgroundImage:bg}}>
    //             <img src={Theme.icons.logo} />
    //             {children}
    //         </div>
    //     </>
    // )
    if(isLoad) return (
        <div id="login-layout" style={{backgroundImage:bg}}>
                 <img src={Theme.icons.logo} />
                 {children}
             </div>
    )
    return <Splash />
}