import React, { ReactNode } from "react";
import {BASE_ADDRESS} from '../../environments';
import PanelNavigation from './nav';
export default ({children}: any) => {
     return(
        <>            
            <div id="panel-layout">
                <PanelNavigation />
                <div id="panel-container">
                    {children}
                </div>
            </div>
        </>
    )
}