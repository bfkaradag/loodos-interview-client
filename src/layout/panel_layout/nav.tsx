import { InputGroup, InputLeftAddon, Input, InputRightAddon } from "@chakra-ui/react";
import SearchInput from "../../components/SearchInput";
import { BASE_ADDRESS } from "../../environments";
import NavRight from './side_bar';
import Theme from '../../theme';
import { useNavigate } from "react-router";

export default () => {
    const navigate = useNavigate();

    return(
        <header>
            <div className="nav-logo d-flex row" style={{cursor:"pointer"}} onClick = {() => navigate("/panel")}>
                <img src={Theme.icons.logo} />
                <img src={Theme.icons.movieflix} />
            </div>
            <div className="nav-search">
                <SearchInput />
            </div>
            <div className="nav-username">
                <p>{sessionStorage.getItem("firstName") + " " + sessionStorage.getItem("lastName")}</p>
            </div>
            <div className="nav-nav">
                <NavRight />
            </div>
        </header>
    )
}