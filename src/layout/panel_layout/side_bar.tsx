import {useState} from 'react';
import { IconButton } from '@chakra-ui/button';
import {HamburgerIcon} from '@chakra-ui/icons';
import Theme from '../../theme';
import { useNavigate } from 'react-router';
export default () => {
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const handleClick = () => setOpen(!open);

    return(
        <>
            <IconButton 
                variant="text"
                aria-label= "Open navigation"
                icon = {<HamburgerIcon display = {!open ? "flex" : 'none'} />}
                color="white"
                fontSize="26px"
                onClick = {handleClick}
            />
        {
            open
            ?
            <nav>
            <div className="side-bar d-flex col ">
                <div className="bar-close">
                <IconButton 
                        aria-label="Bar close"
                        icon = {<img src={Theme.icons.close} />}
                        variant={"text"}
                        onClick = {handleClick}
                    />
                </div>
                <div className="bar-options d-flex col">
                    <IconButton 
                        aria-label="Bar home"
                        icon = {<img src={Theme.icons.house} />}
                        variant={"text"}
                    />
                    <IconButton 
                        aria-label="Bar home"
                        icon = {<img src={Theme.icons.filmstrip} />}
                        variant={"text"}
                    />
                    <IconButton 
                        aria-label="Bar home"
                        icon = {<img src={Theme.icons.tv} />}
                        variant={"text"}
                    />
                    <IconButton 
                        aria-label="Bar home"
                        icon = {<img src={Theme.icons.heartFill} />}
                        variant={"text"}
                        onClick = {() => navigate("/panel/favourites")}
                    />
                </div>
            </div>
            </nav>
            :
            null
        }
        </>
    )
}