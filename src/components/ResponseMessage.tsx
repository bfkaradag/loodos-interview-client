import { Alert, AlertIcon, AlertStatus } from "@chakra-ui/alert";
import { useEffect, useState } from "react";

export default (props: any) => {
    if(props.show){
    const [status, setStatus] = useState<AlertStatus>();
    useEffect(() => {
        switch(props.response.status){
            case 200: setStatus("success"); break;
            case 404: setStatus("error"); break;
            case 403: setStatus("error"); break;
            case 409: setStatus("error"); break;
            
        }
    },[])
    return(
        <Alert status={status} >
            <AlertIcon />
            {
                props.response.statusText
            }
        </Alert> 
        )
    }
    return null;
}