import Theme from "../theme";

export default () => {
    return(
        <div className="App-header">
            <img src={Theme.icons.logo} className="App-logo" />
        </div>
    )
}