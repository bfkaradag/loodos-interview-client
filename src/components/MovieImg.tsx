export default (props: any) => {
    const base = "https://www.themoviedb.org/t/p/w440_and_h660_face";
    return <img loading="lazy" src={base + props.url} />
}