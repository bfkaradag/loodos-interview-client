import { InputGroup, InputLeftAddon, Input, InputRightAddon } from "@chakra-ui/input";
import {SearchIcon} from '@chakra-ui/icons';
import { Select } from "@chakra-ui/react";
import {useState} from 'react';
import { ThemeProvider } from "@emotion/react";
import { useNavigate } from "react-router-dom";

export default () => {
    const navigate = useNavigate();
    
    const CategorySelect = () => {
        return (
            <Select paddingInlineEnd={0} border={"none"} style={{fontSize: 12}} placeholder="All">
                <option value="option1">Option 1</option>
                <option value="option2">Option 2</option>
                <option value="option3">Option 3</option>
            </Select>
        )
    }

    const [searchWord, setSearchWord] = useState("");
    const handleWordChange = (e: any) => setSearchWord(e.target.value);
    const searchMovie = () => {
        return navigate(`/panel/search?query=${searchWord}`)
    }

    return(
        <InputGroup size="lg" w={"100%"}>
            <InputLeftAddon 
                paddingInlineEnd = {0} 
                paddingInlineStart = {0} 
                className="text-input" 
                children={<CategorySelect />} 
            />
            <Input className="text-input" placeholder="Search" onChange={handleWordChange} />
            <InputRightAddon 
                className="text-input" 
                children={<SearchIcon fontSize="large" />} 
                marginStart= {0}
                onClick = {searchMovie}
                disabled = {searchWord.length > 0}
                />
        </InputGroup>
    )
}