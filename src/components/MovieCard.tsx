import {CheckIcon} from '@chakra-ui/icons';
import { IconButton } from '@chakra-ui/button';
import Movie from "../models/Movie";
import Theme from '../theme';
import MovieImg from './MovieImg';
import MovieService from '../services/movieService';
import { useState } from 'react';

export default (movie: Movie) => {
    const [favouriteState, setFavouriteState] = useState(movie.isFavourite);
    const handleFavourite = () => {
        MovieService.favourite(movie.id)
        .then(d => setFavouriteState(!favouriteState))
        .catch(err => console.error(err))
    }
    return(
        <div className="movie-card d-flex col" key= {movie.id}>
            <MovieImg url = {movie.imgUrl} />
            <div className="movie-card-info d-flex row">
                <div className="col" style={{textAlign:"left"}}>
                    <h3>{movie.title ? movie?.title.length > 12 ? movie?.title.substring(0, 12) + "..." : movie?.title: null}</h3>
                    <p>{movie.releaseDate?.substring(0,4)}</p>
                </div>
                <IconButton 
                    _hover = {favouriteState ? {backgroundColor:"gray"} : {backgroundColor: "#e83c3c"}}
                    aria-label = "Add movie to favourites"
                    icon = {<img src={Theme.icons.heart} />}
                    background = {favouriteState? "#e83c3c" : "gray"}
                    rounded = "50%"
                    onClick = {handleFavourite}
                />
            </div>
        </div>
    )
}