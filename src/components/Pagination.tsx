import {ChevronRightIcon, ChevronLeftIcon} from '@chakra-ui/icons';
import { Button, IconButton } from '@chakra-ui/button';
import { useEffect, useState } from 'react';


export default (props: any) => {
    const [selected, setSelected] = useState(1);
    const [disabled, setDisabled] = useState(false);
    useEffect(() => {
        setDisabled(true)
        const handlePageChange = async (numb: number) => {
            await props.handlePagination(numb);
            setSelected(numb)
        };
        handlePageChange(selected);
        setTimeout(() => {
            setDisabled(false);
        }, 1000);
    }, [selected])

    const goPrevious = () => setSelected(selected - 1);
    const goNext = () => setSelected(selected + 1);
    const handleClick = (numb: number) => setSelected(numb);
    const buttons = [];
    var max = selected + 7 > props.count ? props.count : selected + 7;
    var min = selected + 7 > props.count ? max - 7 : selected;
    for (let i = min; i <= max; i++) {
        buttons.push(
        <li key={i} value = {i}>
            <a onClick={() => handleClick(i)} className={selected == i ? "pagination-page-btn pagination-page-btn-selected" : "pagination-page-btn"}>
                {i}
            </a>
        </li>)
        
    }
    return(
        <div className="pagination d-flex row">
            <IconButton 
                aria-label="Paginaton previous"
                icon = {<ChevronLeftIcon />}
                variant="text"
                color="white"
                className="pagination-arrow"
                disabled = {selected == 1 || disabled}
                onClick = {goPrevious}
            />
            <ul className="d-flex row">
                {buttons}
            </ul>
            <IconButton 
                aria-label="Paginaton next"
                icon = {<ChevronRightIcon />}
                variant="text"
                color="white"
                className="pagination-arrow"
                disabled = {selected == props.count ||  disabled}
                onClick = {goNext}
            />
        </div>
    )
}
